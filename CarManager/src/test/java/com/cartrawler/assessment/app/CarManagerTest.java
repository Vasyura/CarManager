package com.cartrawler.assessment.app;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

import org.junit.Test;

import com.cartrawler.assessment.car.CarResult;
import com.cartrawler.assessment.car.CarResult.FuelPolicy;

/*
 * Implement tests for car sorting algorithm verification.
 */
public class CarManagerTest {

  @Test
  public void test1() {
    CarResult[] testData = {
        new CarResult("Volkswagen Polo","DELPASO", "EDMR", 10.00, FuelPolicy.FULLFULL),
        new CarResult("Peugeot 107","SIXT", "MCMR", 22.50,  FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Golf", "FIREFLY", "CDMR", 48.00, FuelPolicy.FULLFULL),
        new CarResult("Citroen Berlingo", "GOLDCAR",  "CMMV", 28.00,FuelPolicy.FULLFULL),
        new CarResult("Citroen Berlingo", "GOLDCAR",  "CMMV", 28.00,FuelPolicy.FULLFULL),
        new CarResult("Citroen Berlingo", "GOLDCAR", "CMMV", 28.00,FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Up", "DELPASO", "MDMR", 8.00, FuelPolicy.FULLEMPTY),
        new CarResult("Opel Corsa", "SIXT", "EDMN", 29.50, FuelPolicy.FULLFULL),
        new CarResult("Citroen Berlingo", "GOLDCAR", "CMMV", 28.00,FuelPolicy.FULLFULL),
        new CarResult("Mercedes A Class", "AVIS", "ICAV", 80.00, FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Golf", "GOLDCAR", "CLMR", 18.00, FuelPolicy.FULLEMPTY),
        new CarResult("Peugeot 107", "AVIS", "MCMR", 30.05, FuelPolicy.FULLFULL),
        new CarResult("Ford Galaxy", "RECORD", "FVAR", 65.00, FuelPolicy.FULLEMPTY)
    };
    CarResult[] expectedOrderedData = {
        new CarResult("Peugeot 107", "SIXT", "MCMR", 22.50,  FuelPolicy.FULLFULL),
        new CarResult("Peugeot 107","AVIS",  "MCMR", 30.05, FuelPolicy.FULLFULL),
        new CarResult("Opel Corsa", "SIXT", "EDMN", 29.50, FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Golf", "FIREFLY", "CDMR", 48.00, FuelPolicy.FULLFULL),
        new CarResult("Mercedes A Class", "AVIS", "ICAV", 80.00, FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Up", "DELPASO", "MDMR", 8.00, FuelPolicy.FULLEMPTY),
        new CarResult("Volkswagen Polo", "DELPASO", "EDMR", 10.00, FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Golf", "GOLDCAR", "CLMR", 18.00, FuelPolicy.FULLEMPTY),
        new CarResult("Citroen Berlingo", "GOLDCAR", "CMMV", 28.00,FuelPolicy.FULLFULL),
        new CarResult("Ford Galaxy", "RECORD", "FVAR", 65.00, FuelPolicy.FULLEMPTY)
    };
    AssessmentRunner runner = new AssessmentRunner();
    LinkedHashSet<CarResult> data = new LinkedHashSet<CarResult>(testData.length); 
    Collections.addAll(data, testData);
    List<CarResult> orderedList = runner.orderCars(data);
    for (int i = 0; i < expectedOrderedData.length; i++) {
      assertEquals("Item: " + i, expectedOrderedData[i], orderedList.get(i));
    }

  }
  
  @Test
  public void test2() {
    CarResult[] testData = {
        new CarResult("Volkswagen Polo","DELPASO", "EDMR", 10.00, FuelPolicy.FULLFULL),
        new CarResult("Peugeot 107","SIXT", "MCMR", 22.50,  FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Golf", "FIREFLY", "CDMR", 48.00, FuelPolicy.FULLFULL),
        new CarResult("Citroen Berlingo", "GOLDCAR",  "CMMV", 28.00,FuelPolicy.FULLFULL),
        new CarResult("Citroen", "GOLDCAR", "CMMV", 15.00,FuelPolicy.FULLFULL),
        new CarResult("Opel Corsa", "SIXT", "EDMN", 29.50, FuelPolicy.FULLFULL),
        new CarResult("Mercedes A Class", "AVIS", "ICAV", 80.00, FuelPolicy.FULLFULL),
    };
    CarResult[] expectedOrderedData = {
        new CarResult("Peugeot 107","SIXT", "MCMR", 22.50,  FuelPolicy.FULLFULL),
        new CarResult("Opel Corsa", "SIXT", "EDMN", 29.50, FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Golf", "FIREFLY", "CDMR", 48.00, FuelPolicy.FULLFULL),
        new CarResult("Mercedes A Class", "AVIS", "ICAV", 80.00, FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Polo","DELPASO", "EDMR", 10.00, FuelPolicy.FULLFULL),
        new CarResult("Citroen", "GOLDCAR", "CMMV", 15.00,FuelPolicy.FULLFULL),
        new CarResult("Citroen Berlingo", "GOLDCAR",  "CMMV", 28.00,FuelPolicy.FULLFULL),
    };
    AssessmentRunner runner = new AssessmentRunner();
    LinkedHashSet<CarResult> data = new LinkedHashSet<CarResult>(testData.length); 
    Collections.addAll(data, testData);
    List<CarResult> orderedList = runner.orderCars(data);
    assertEquals(expectedOrderedData.length,  orderedList.size());
    for (int i = 0; i < expectedOrderedData.length; i++) {
      assertEquals("Item: " + i, expectedOrderedData[i], orderedList.get(i));
    }
  }

  @Test
  public void test3() {
    CarResult[] testData = {
        new CarResult("Peugeot 107","ENTERPRISE","MCMR",78.1,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Peugeot 107","SIXT","MCMR",97.08,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Smart Fortwo","SIXT","MUMR",115.76,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Peugeot 107","AVIS","MCMR",145.52,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Polo","FIREFLY","EDMR",29.79,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Volkswagen Golf","FIREFLY","CDMR",33.84,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Volkswagen Polo","SIXT","EDMR",128.93,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Citroen Berlingo","FIREFLY","CMMV",35.67,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Renault Clio Estate","FIREFLY","EWMR",45.16,CarResult.FuelPolicy.FULLEMPTY)
    };
    CarResult[] expectedOrderedData = {
        new CarResult("Peugeot 107","ENTERPRISE","MCMR",78.1,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Peugeot 107","SIXT","MCMR",97.08,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Smart Fortwo","SIXT","MUMR",115.76,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Peugeot 107","AVIS","MCMR",145.52,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Polo","FIREFLY","EDMR",29.79,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Renault Clio Estate","FIREFLY","EWMR",45.16,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Volkswagen Polo","SIXT","EDMR",128.93,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Golf","FIREFLY","CDMR",33.84,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Citroen Berlingo","FIREFLY","CMMV",35.67,CarResult.FuelPolicy.FULLEMPTY),
    };
    AssessmentRunner runner = new AssessmentRunner();
    LinkedHashSet<CarResult> data = new LinkedHashSet<CarResult>(testData.length); 
    Collections.addAll(data, testData);
    List<CarResult> orderedList = runner.orderCars(data);
    assertEquals(expectedOrderedData.length,  orderedList.size());
    for (int i = 0; i < expectedOrderedData.length; i++) {
      assertEquals("Item: " + i, expectedOrderedData[i], orderedList.get(i));
    }
  }

  @Test
  public void test4() {
    CarResult[] testData = {
        new CarResult("Peugeot 107","ENTERPRISE","IPMGR",78.1,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Peugeot 107","SIXT","MCMR",97.08,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Smart Fortwo","SIXT","MUMR",115.76,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Peugeot 107","AVIS","MCMR",145.52,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Polo","FIREFLY","KDMR",29.79,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Volkswagen Golf","FIREFLY","LDMR",33.84,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Volkswagen Polo","SIXT","SDMR",128.93,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Citroen Berlingo","FIREFLY","CMMV",35.67,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Renault Clio Estate","FIREFLY","EWMR",45.16,CarResult.FuelPolicy.FULLEMPTY)
    };
    CarResult[] expectedOrderedData = {
        new CarResult("Peugeot 107","SIXT","MCMR",97.08,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Smart Fortwo","SIXT","MUMR",115.76,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Peugeot 107","AVIS","MCMR",145.52,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Renault Clio Estate","FIREFLY","EWMR",45.16,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Citroen Berlingo","FIREFLY","CMMV",35.67,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Volkswagen Polo","FIREFLY","KDMR",29.79,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Volkswagen Golf","FIREFLY","LDMR",33.84,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Peugeot 107","ENTERPRISE","IPMGR",78.1,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Polo","SIXT","SDMR",128.93,CarResult.FuelPolicy.FULLFULL),
    };
    AssessmentRunner runner = new AssessmentRunner();
    LinkedHashSet<CarResult> data = new LinkedHashSet<CarResult>(testData.length); 
    Collections.addAll(data, testData);
    List<CarResult> orderedList = runner.orderCars(data);
    assertEquals(expectedOrderedData.length,  orderedList.size());
    for (int i = 0; i < expectedOrderedData.length; i++) {
      assertEquals("Item: " + i, expectedOrderedData[i], orderedList.get(i));
    }
  }

}
