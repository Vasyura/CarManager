/**
 * 
 */
package com.cartrawler.assessment.app;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.cartrawler.assessment.car.CarResult;
import com.cartrawler.assessment.car.CarResult.FuelPolicy;

/**
 * Test removing full fill car from the sorted ist.
 */
public class CarRemoveTest {

  @Test
  public void testMedianCalculation() {
    AssessmentRunner runner = new AssessmentRunner();
    ArrayList<CarResult> cars = new ArrayList<CarResult>(); 
    cars.add(new CarResult("Peugeot 107","ENTERPRISE","MCMR",78.1,CarResult.FuelPolicy.FULLFULL));
    double median = runner.calculateMedianPrice(cars);
    assertEquals(Double.valueOf(78.1), Double.valueOf(median));
    cars.add(new CarResult("Peugeot 107","ENTERPRISE","MCMR",90.1,CarResult.FuelPolicy.FULLFULL));
    median = runner.calculateMedianPrice(cars);
    assertEquals(Double.valueOf((78.1 + 90.1)/2), Double.valueOf(median));
    cars.add(new CarResult("Peugeot 107","ENTERPRISE","MCMR",100.1,CarResult.FuelPolicy.FULLFULL));
    median = runner.calculateMedianPrice(cars);
    assertEquals(Double.valueOf(90.1), Double.valueOf(median));
  }

  @Test
  public void testRemoveFullCars1() {
    CarResult[] testData = {
        new CarResult("Peugeot 107","ENTERPRISE","MCMR",78.1,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Peugeot 107","SIXT","MCMR",97.08,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Polo","FIREFLY","EDMR",29.79,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Renault Clio Estate","FIREFLY","EWMR",45.16,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Volkswagen Golf","FIREFLY","CDMR",33.84,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Citroen Berlingo","FIREFLY","CMMV",35.67,CarResult.FuelPolicy.FULLEMPTY),
    };
    
    CarResult[] expectedData = {
        new CarResult("Peugeot 107","ENTERPRISE","MCMR",78.1,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Polo","FIREFLY","EDMR",29.79,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Renault Clio Estate","FIREFLY","EWMR",45.16,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Volkswagen Golf","FIREFLY","CDMR",33.84,CarResult.FuelPolicy.FULLEMPTY),
        new CarResult("Citroen Berlingo","FIREFLY","CMMV",35.67,CarResult.FuelPolicy.FULLEMPTY),
    };
    AssessmentRunner runner = new AssessmentRunner();
    List<CarResult> result = runner.removeFullFillCars(Arrays.asList(testData));
    assertEquals(expectedData.length, result.size());
    for(int i = 0; i < expectedData.length; i++) {
      assertEquals("Item: " + i, expectedData[i], result.get(i));
    }
  }

  @Test
  public void testRemoveFullCars2() {
    CarResult[] testData = {
        new CarResult("Peugeot 107","ENTERPRISE","MCMR",78.1,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Peugeot 107","SIXT","MCMR",97.08,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Polo","FIREFLY","EDMR",29.79,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Renault Clio Estate","FIREFLY","EWMR",45.16,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Golf","FIREFLY","CDMR",33.84,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Citroen Berlingo","FIREFLY","CMMV",35.67,CarResult.FuelPolicy.FULLFULL),
    };
    
    CarResult[] expectedData = {
        new CarResult("Peugeot 107","ENTERPRISE","MCMR",78.1,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Polo","FIREFLY","EDMR",29.79,CarResult.FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Golf","FIREFLY","CDMR",33.84,CarResult.FuelPolicy.FULLFULL),
    };
    AssessmentRunner runner = new AssessmentRunner();
    List<CarResult> result = runner.removeFullFillCars(Arrays.asList(testData));
    assertEquals(expectedData.length, result.size());
    for(int i = 0; i < expectedData.length; i++) {
      assertEquals("Item: " + i, expectedData[i], result.get(i));
    }
  }

  @Test
  public void testRemoveFullCars3() {
    CarResult[] testData = {
        new CarResult("Peugeot 107", "SIXT", "MCMR", 22.50,  FuelPolicy.FULLFULL),
        new CarResult("Peugeot 107","AVIS",  "MCMR", 30.05, FuelPolicy.FULLFULL),
        new CarResult("Peugeot 108","AVIS",  "MCMR", 36.05, FuelPolicy.FULLFULL),
        new CarResult("Opel Corsa", "SIXT", "EDMN", 29.50, FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Golf", "FIREFLY", "CDMR", 48.00, FuelPolicy.FULLFULL),
        new CarResult("Mercedes A Class", "AVIS", "ICAV", 80.00, FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Up", "DELPASO", "MDMR", 8.00, FuelPolicy.FULLEMPTY),
        new CarResult("Volkswagen Polo", "DELPASO", "EDMR", 10.00, FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Golf", "GOLDCAR", "CLMR", 18.00, FuelPolicy.FULLEMPTY),
        new CarResult("Citroen Berlingo", "GOLDCAR", "CMMV", 28.00,FuelPolicy.FULLFULL),
        new CarResult("Ford Galaxy", "RECORD", "FVAR", 65.00, FuelPolicy.FULLEMPTY)
    };
    
    CarResult[] expectedData = {
        new CarResult("Peugeot 107", "SIXT", "MCMR", 22.50,  FuelPolicy.FULLFULL),
        new CarResult("Peugeot 107","AVIS",  "MCMR", 30.05, FuelPolicy.FULLFULL),
        new CarResult("Opel Corsa", "SIXT", "EDMN", 29.50, FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Golf", "FIREFLY", "CDMR", 48.00, FuelPolicy.FULLFULL),
        new CarResult("Mercedes A Class", "AVIS", "ICAV", 80.00, FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Up", "DELPASO", "MDMR", 8.00, FuelPolicy.FULLEMPTY),
        new CarResult("Volkswagen Polo", "DELPASO", "EDMR", 10.00, FuelPolicy.FULLFULL),
        new CarResult("Volkswagen Golf", "GOLDCAR", "CLMR", 18.00, FuelPolicy.FULLEMPTY),
        new CarResult("Ford Galaxy", "RECORD", "FVAR", 65.00, FuelPolicy.FULLEMPTY)
    };
    AssessmentRunner runner = new AssessmentRunner();
    List<CarResult> result = runner.removeFullFillCars(Arrays.asList(testData));
    assertEquals(expectedData.length, result.size());
    for(int i = 0; i < expectedData.length; i++) {
      assertEquals("Item: " + i, expectedData[i], result.get(i));
    }
  }

}
