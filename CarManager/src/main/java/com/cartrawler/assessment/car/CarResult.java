package com.cartrawler.assessment.car;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class CarResult {
    private final String description;
    private final String supplierName;
    private final String sippCode;
    private final double rentalCost;
    private final FuelPolicy fuelPolicy;
    
	public enum FuelPolicy {
        FULLFULL,
        FULLEMPTY};
    
    public CarResult(String description, String supplierName, String sipp, double cost, FuelPolicy fuelPolicy) {
        this.description = description;
        this.supplierName = supplierName;
        this.sippCode = sipp;
        this.rentalCost = cost;
        this.fuelPolicy = fuelPolicy;
    }
    
    public String getDescription() {
        return this.description;        
    }
    
    public String getSupplierName() {
        return this.supplierName;        
    }
    
    public String getSippCode() {
        return this.sippCode;        
    }
    
    public double getRentalCost() {
        return this.rentalCost;        
    }
    
    public FuelPolicy getFuelPolicy() {
        return this.fuelPolicy;
    }

    @Override
    public String toString() {
         /*     return " new CarResult(\"" + this.description + "\",\"" +
      this.supplierName + "\",\"" +
      this.sippCode + "\"," +
      this.rentalCost + ",CarResult.FuelPolicy." +
      this.fuelPolicy + ")";*/

             return this.supplierName + " : " +
            this.description + " : " +
            this.sippCode + " : " +
            this.rentalCost + " : " +
            this.fuelPolicy;
    }

	@Override
	public int hashCode() {
	    return new HashCodeBuilder(17, 37)
            .append(this.description)
            .append(this.fuelPolicy)
            .append(this.rentalCost)
            .append(this.sippCode)
            .append(this.supplierName)
            .toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
	    if (obj == null || (getClass() != obj.getClass())) {
	      return false;
	    } else {
          if (this == obj)  {
            return true;
          }
	      final CarResult o = (CarResult) obj;
	      return new EqualsBuilder()
	          .append(this.description, o.description)
	          .append(this.fuelPolicy, o.fuelPolicy)
	          .append(this.rentalCost, o.rentalCost)
	          .append(this.sippCode, o.sippCode)
	          .append(this.supplierName, o.supplierName)
	          .isEquals();
	    }
	}

}
