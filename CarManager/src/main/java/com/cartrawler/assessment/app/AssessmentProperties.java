/**
 * 
 */
package com.cartrawler.assessment.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

/**
 * Class to present configuration properties.
 * It can be loaded from any place including DB or file.
 * This version assumes hardcoded properties.
 */
public class AssessmentProperties {

    private final String[] CORPORATE_SUPLIERS = {
        "AVIS", "BUDGET", "ENTERPRISE", "FIREFLY", "HERTZ", "SIXT", "THRIFTY"
    };

    private ArrayList<String> corporateSuppliers;

	public AssessmentProperties() {
	  corporateSuppliers = new ArrayList<String>();
	  Collections.addAll(corporateSuppliers, CORPORATE_SUPLIERS);
	}

	/**
	 * Check if supplier for the car is in corporate sector.
	 * @param supplierName 
	 * @return true if corporate car
	 */
	public boolean isCorporateCar(String supplierName) {
	  Optional<String> supplier = corporateSuppliers.stream().filter(sp -> sp.equals(supplierName)).findFirst();
	  if (supplier.isPresent()) {
	    return true;
	  }
	  return false;
	}
}
