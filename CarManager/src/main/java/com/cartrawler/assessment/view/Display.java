package com.cartrawler.assessment.view;

import com.cartrawler.assessment.car.CarResult;

import java.util.Collection;

public class Display {
    public void render(Collection<CarResult> cars) {
        for (CarResult car : cars) {
            System.out.println (car);
        }
    }
}
